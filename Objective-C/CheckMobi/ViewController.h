//
//  ViewController.h
//  CheckMobi
//
//  Created by checkMobi on 14/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property(nonatomic, weak) IBOutlet UITableView *settingsTable;
@property(nonatomic) BOOL fromMain;
          
@end

