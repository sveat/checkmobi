//
//  IntroViewController.m
//  CheckMobi
//
//  Created by checkMobi on 18/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "IntroViewController.h"
#import "HexColors.h"
#import <KVNProgress/KVNProgress.h>

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_continueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_continueBtn setTitleColor:[UIColor colorWithWhite:0 alpha:0.8] forState:UIControlStateDisabled];
    _continueBtn.backgroundColor = [UIColor colorWithHexString:DEFAULT_COLOR];
    _continueBtn.layer.cornerRadius = 5;
    _continueBtn.clipsToBounds = YES;
    _continueBtn.titleLabel.font = [UIFont fontWithName:NORMAL_FONT size:16];
    [_continueBtn addTarget:self action:@selector(goToSettings) forControlEvents:UIControlEventTouchUpInside];
    
    [_createAccount addTarget:self action:@selector(goToSite) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *swipe = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:swipe];
}

- (void)resignOnSwipe:(id)sender {
    [_apiKey resignFirstResponder];
}

- (void)goToSettings {
    if (_apiKey.text.length > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:_apiKey.text forKey:API_KEY];
        [self performSegueWithIdentifier:@"GoToSettings" sender:self];
    } else {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"ApiKeyError", @"")];
    }
}

- (void)goToSite {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.checkmobi.com/login.html#/signup"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
