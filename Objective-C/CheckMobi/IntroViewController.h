//
//  IntroViewController.h
//  CheckMobi
//
//  Created by checkMobi on 18/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(nonatomic, weak) IBOutlet UITextField *apiKey;
@property(nonatomic, weak) IBOutlet UIButton *continueBtn;
@property(nonatomic, weak) IBOutlet UIButton *createAccount;

@end
