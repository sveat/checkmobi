//
//  AFAPIClient.m
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "CheckMobiAPI.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "JSONResponseSerializerWithData.h"

@implementation CheckMobiAPI

+ (instancetype)sharedClient {
    static CheckMobiAPI *_sharedClient = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[CheckMobiAPI alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        
        _sharedClient.requestSerializer = [AFJSONRequestSerializer new];
        _sharedClient.responseSerializer = [JSONResponseSerializerWithData new];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [_sharedClient.requestSerializer setValue:[defaults valueForKey:API_KEY] forHTTPHeaderField:@"Authorization"];
    });
    
    return _sharedClient;
}

- (void)validateCallNumber:(NSMutableDictionary*)dict andResponse:(void (^)(id result, NSError *error))block {
    [self POST:API_CHECK_NUMBER parameters:dict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* jsonResponse = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        NSLog(@"validateCallNumber %@", jsonResponse);
        
        NSString *formatting = [jsonResponse valueForKey:@"formatting"];
        if (formatting != nil) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setValue:formatting forKey:PHONE_NUMBER];
        }
        
        block(jsonResponse, nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

- (void)validateRequest:(NSMutableDictionary*)dict andResponse:(void (^)(id result, NSError *error))block {
    [self POST:API_VALIDATION parameters:dict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* jsonResponse = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        NSLog(@"validateRequest %@", jsonResponse);
        
        block(jsonResponse, nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

- (void)validateVerify:(NSMutableDictionary*)dict andResponse:(void (^)(id result, NSError *error))block {
    [self POST:API_VALIDATION_VERIFY parameters:dict success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* jsonResponse = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        NSLog(@"validateRequest %@", jsonResponse);
        
        block(jsonResponse, nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

- (void)getValidationStatus:(NSString*)validationKey andResponse:(void (^)(id result, NSError *error))block {
    [self GET:[NSString stringWithFormat:@"%@%@", API_VALIDATION_STATUS, validationKey] parameters:nil success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
        NSDictionary* jsonResponse = [NSJSONSerialization JSONObjectWithData:operation.responseData options:kNilOptions error:nil];
        NSLog(@"getValidationStatus %@", jsonResponse);
        
        block(jsonResponse, nil);
    } failure:^(AFHTTPRequestOperation * _Nonnull operation, NSError * _Nonnull error) {
        block(nil, error);
    }];
}

@end
