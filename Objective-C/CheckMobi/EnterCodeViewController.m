//
//  EnterCodeViewController.m
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "EnterCodeViewController.h"
#import "HexColors.h"
#import "CheckMobiAPI.h"
#import "JSONResponseSerializerWithData.h"
#import <KVNProgress/KVNProgress.h>

@interface EnterCodeViewController ()

@end

@implementation EnterCodeViewController {
    int noOfRetriesSMS;
    int noOfRetriesIVR;
    BOOL triedSMS;
    BOOL triedIVR;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.title = [defaults valueForKey:PHONE_NUMBER];
    _input.keyboardType = UIKeyboardTypeNumberPad;
    
    [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn setTitleColor:[UIColor colorWithWhite:0 alpha:0.8] forState:UIControlStateDisabled];
    _btn.backgroundColor = [UIColor colorWithHexString:DEFAULT_COLOR];
    _btn.layer.cornerRadius = 5;
    _btn.clipsToBounds = YES;
    [_btn setTitle:NSLocalizedString(@"ValidatePin", @"") forState:UIControlStateNormal];
    _btn.titleLabel.font = [UIFont fontWithName:NORMAL_FONT size:19];
    [_btn addTarget:self action:@selector(validatePin) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *swipe = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignOnSwipe:)];
    self.view.userInteractionEnabled = YES;
    [self.view addGestureRecognizer:swipe];
    
    [_resend addTarget:self action:@selector(resendSMS) forControlEvents:UIControlEventTouchUpInside];
    [_call addTarget:self action:@selector(callMeWithCode) forControlEvents:UIControlEventTouchUpInside];

    UIImage *backBtnImage = [UIImage imageNamed:@"icon-back"];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 20, backBtnImage.size.width + 10, 44);
    [closeButton setImage:backBtnImage forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    self.navigationItem.leftBarButtonItem = back;
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    triedSMS = NO || (![defaults valueForKey:ENABLED_BACKUP_SMS]);
    triedIVR = NO || (![defaults valueForKey:ENABLED_BACKUP_IVR]);
    noOfRetriesSMS = [[defaults valueForKey:SMS_NO_OF_RETRIES] intValue];
    noOfRetriesIVR = [[defaults valueForKey:IVR_NO_OF_RETRIES] intValue];
    
    if ([_validationMethod isEqualToString:API_CHECK_SMS]) {
        _msg.text = NSLocalizedString(@"SendSMSWait", @"");
        _errorMsg.text = NSLocalizedString(@"NotReceivedSMS", @"");
        triedSMS = YES;
        
        noOfRetriesSMS--;
        if (noOfRetriesSMS > 0) {
            [self startCountDown];
        } else {
            _resend.hidden = YES;
        }
    } else if ([_validationMethod isEqualToString:API_CHECK_IVR]) {
        _errorMsg.text = NSLocalizedString(@"NotReceivedCall", @"");
        _msg.text = NSLocalizedString(@"ReceiveCallCode", @"");
        triedIVR = YES;
        noOfRetriesIVR--;
    } else {
        _errorMsg.text = NSLocalizedString(@"NotReceivedCall", @"");
        _msg.text = NSLocalizedString(@"MissedCallInfo", @"");
    }
    
    if ((![defaults boolForKey:ENABLED_BACKUP_IVR]) && (![defaults boolForKey:ENABLED_BACKUP_SMS])) {
        _errorMsg.text = @"";
    }
    
    if ([defaults boolForKey:ENABLED_BACKUP_IVR]) {
        _call.hidden = NO;
    } else {
        _call.hidden = YES;
    }
    
    if ([defaults boolForKey:ENABLED_BACKUP_SMS]) {
        _resend.hidden = NO;
    } else {
        _resend.hidden = YES;
    }
    
    if ((_call.hidden) && (!_resend.hidden)) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_resend attribute:NSLayoutAttributeCenterX
                                                        relatedBy:NSLayoutRelationEqual
                                                           toItem:self.view attribute:NSLayoutAttributeCenterX
                                                       multiplier:1.0f constant:0]];
    } else if ((!_call.hidden) && (_resend.hidden)) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_call attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0f constant:0]];
    }
}

- (void)resignOnSwipe:(id)sender {
    [_input resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark counter

- (void)startCountDown {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    int noOfSeconds = 0;
    noOfSeconds = [[defaults valueForKey:SMS_RETRY_IN] intValue];
    
    self.countingDown = YES;
    self.resend.enabled = NO;
    int seconds = noOfSeconds;
    [self countDownFor:seconds];
}

- (void)countDownFor:(int)seconds {
    if (seconds == 0) {
        self.countingDown = NO;
        self.resend.enabled = YES;
        [self.resend setTitle:NSLocalizedString(@"ResendSMS", @"") forState:UIControlStateNormal];
        
        return;
    } else {
        [self.resend setTitle:[NSString stringWithFormat:@"Resend SMS in %d", seconds] forState:UIControlStateNormal];
    }
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self countDownFor:(seconds - 1)];
    });
}

#pragma mark API

- (void)resendSMS {
    NSLog(@"resendSMS: %d %d", noOfRetriesSMS, noOfRetriesIVR);
    
    _msg.text = NSLocalizedString(@"SendSMSWait", @"");
    _errorMsg.text = NSLocalizedString(@"NotReceivedSMS", @"");
    
    noOfRetriesSMS--;
    [self codeVerificationMethods:API_CHECK_SMS];
    
    if (noOfRetriesSMS > 0) {
        [self startCountDown];
    } else {
        _resend.hidden = YES;
    }
}

- (void)callMeWithCode {
    NSLog(@"callMeWithCode: %d %d", noOfRetriesSMS, noOfRetriesIVR);
    
    _errorMsg.text = NSLocalizedString(@"NotReceivedCall", @"");
    _msg.text = NSLocalizedString(@"ReceiveCallCode", @"");
    _call.enabled = NO;
    
    noOfRetriesIVR--;
    [self codeVerificationMethods:API_CHECK_IVR];
    
    if (noOfRetriesIVR <= 0) {
        _call.hidden = YES;
    }
}

- (void)codeVerificationMethods:(NSString*)typeCode {
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        
        if ([typeCode isEqualToString:API_CHECK_IVR]) {
            _call.enabled = YES;
            noOfRetriesIVR++;
            
            if (noOfRetriesIVR == 1) {
                _call.hidden = NO;
            }
        }
        
        if ([typeCode isEqualToString:API_CHECK_SMS]) {
            noOfRetriesSMS++;
            if (noOfRetriesSMS == 1) {
                [self startCountDown];
                _resend.hidden = NO;
            }
        }
        
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *number = [defaults valueForKey:TYPED_PHONE_NUMBER];
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:typeCode forKey:API_PARAM_TYPE];
    [dict setValue:number forKey:API_PARAM_NUMBER];
    [dict setValue:@"ios" forKey:API_PARAM_PLATFORM];
    
    [[CheckMobiAPI sharedClient] validateRequest:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            _validationId = [result valueForKey:@"id"];
            [KVNProgress dismiss];
            
            if ([typeCode isEqualToString:API_CHECK_IVR]) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    _call.enabled = YES;
                });
            }
        } else {
            if (error != nil) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                if (errorData != nil) {
                    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                    if (serializedData != nil) {
                        NSLog(@"%@", [serializedData valueForKey:@"error"]);
                        [KVNProgress showErrorWithStatus:[serializedData valueForKey:@"error"]];
                    }
                }
            }
            
            if ([typeCode isEqualToString:API_CHECK_IVR]) {
                _call.enabled = YES;
            }
        }
    }];
}

- (void)validatePin {
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        return;
    }
    
    [_input resignFirstResponder];
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:_input.text forKey:@"pin"];
    [dict setValue:_validationId forKey:@"id"];
    
    [[CheckMobiAPI sharedClient] validateVerify:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            if ([[result valueForKey:@"validated"] integerValue] == 1) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [KVNProgress showSuccessWithStatus:[NSString stringWithFormat:@"Validation completed for %@", [defaults valueForKey:PHONE_NUMBER]]];
                [self performSegueWithIdentifier:@"unwindFromValidationViewController" sender:self];
            } else {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
            }
        } else {
            if (error != nil) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                if (errorData != nil) {
                    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                    if (serializedData != nil) {
                        NSLog(@"%@", [serializedData valueForKey:@"error"]);
                    }
                }
            }
            
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
        }
    }];
}

@end
