//
//  ConfirmationViewController.m
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "ConfirmationViewController.h"
#import "HexColors.h"
#import "CheckMobiAPI.h"
#import <KVNProgress/KVNProgress.h>
#import "JSONResponseSerializerWithData.h"
#import "EnterCodeViewController.h"

@interface ConfirmationViewController ()

@end

@implementation ConfirmationViewController {
    BOOL triedSMS;
    BOOL triedIVR;
    BOOL checkInitiated;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.title = [defaults valueForKey:PHONE_NUMBER];
    
    triedSMS = NO || (![defaults valueForKey:ENABLED_BACKUP_SMS]);
    triedIVR = NO || (![defaults valueForKey:ENABLED_BACKUP_IVR]);
    checkInitiated = NO;
    
    _msg.text = @"Verify your number by making a FREE call";
    _info.text = @"We will immediately hang up the call so you won’t be charged";
    
    [_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btn setTitleColor:[UIColor colorWithWhite:0 alpha:0.8] forState:UIControlStateDisabled];
    _btn.backgroundColor = [UIColor colorWithHexString:DEFAULT_COLOR];
    _btn.layer.cornerRadius = 5;
    _btn.clipsToBounds = YES;
    [_btn setTitle:NSLocalizedString(@"CallVerify", @"") forState:UIControlStateNormal];
    _btn.titleLabel.font = [UIFont fontWithName:NORMAL_FONT size:19];
    [_btn addTarget:self action:@selector(validateNumber) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkCallState) name:UIApplicationDidBecomeActiveNotification object:nil];
    [self registerForCalls];
    
    [_sendSMS addTarget:self action:@selector(sendSMSAction) forControlEvents:UIControlEventTouchUpInside];
    [_call addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *backBtnImage = [UIImage imageNamed:@"icon-back"];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 20, backBtnImage.size.width + 10, 44);
    [closeButton setImage:backBtnImage forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    self.navigationItem.leftBarButtonItem = back;
    
    if ([defaults boolForKey:ENABLED_BACKUP_IVR]) {
        _call.hidden = NO;
    } else {
        _call.hidden = YES;
    }
    
    if ([defaults boolForKey:ENABLED_BACKUP_SMS]) {
        _sendSMS.hidden = NO;
    } else {
        _sendSMS.hidden = YES;
    }
    
    if ((_call.hidden) && (!_sendSMS.hidden)) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_sendSMS attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0f constant:0]];
    } else if ((!_call.hidden) && (_sendSMS.hidden)) {
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:_call attribute:NSLayoutAttributeCenterX
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:self.view attribute:NSLayoutAttributeCenterX
                                                             multiplier:1.0f constant:0]];
    }
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name: UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark call utils

- (void)registerForCalls {
    self.callCenter = [[CTCallCenter alloc] init];
    __weak typeof(self) weakSelf = self;
    
    [self.callCenter setCallEventHandler: ^(CTCall* call) {
        if ([call.callState isEqualToString: CTCallStateDialing]) {
             [weakSelf performSelectorOnMainThread:@selector(callInitiated:) withObject:call.callID waitUntilDone:NO];
        }
        else if ([call.callState isEqualToString: CTCallStateDisconnected]) {
             [weakSelf performSelectorOnMainThread:@selector(callCompleted:) withObject:call.callID waitUntilDone:NO];
        }
     }];
}

- (void)callInitiated:(NSString*) callid {
    if (self.dialingNumber == nil || self.validationKey == nil || self.callId != nil) {
        return;
    }
    
    self.callId = callid;
}

- (void)checkCallState {
    NSLog(@"*** checkCallState ***");
    if (self.dialingNumber == nil || self.validationKey == nil || self.callId == nil) {
        return;
    }
    
    bool found = false;
    
    CTCallCenter* center = [[CTCallCenter alloc] init];
    if (center.currentCalls != nil) {
        NSArray* currentCalls = [center.currentCalls allObjects];
        
        for (CTCall *call in currentCalls) {
            if ([call.callID isEqualToString:self.callId]) {
                found = true;
                break;
            }
        }
    }
    
    if (!found) {
        [self callCompleted:self.callId];
    }
}

- (void)callCompleted:(NSString*)call_id {
    if (self.dialingNumber == nil || self.validationKey == nil || self.callId == nil) {
        return;
    }
    
    if (![self.callId isEqualToString:call_id]) {
        return;
    } else {
        if (!checkInitiated) {
            checkInitiated = YES;
            [self checkValidationStatus];
        }
    }
}

#pragma mark API

- (void)checkValidationStatus {
    /*if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:@"No internet connection available!"];
        return;
    }*/
    
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    [[CheckMobiAPI sharedClient] getValidationStatus:_validationKey andResponse:^(id result, NSError *error) {
        if (result != nil) {
            self.dialingNumber = nil;
            self.validationKey = nil;
            self.callId = nil;
            
            NSNumber *validated = [result objectForKey:@"validated"];
            if ((validated != nil) && ([validated boolValue])) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [KVNProgress showSuccessWithStatus:[NSString stringWithFormat:@"Validation completed for %@", [defaults valueForKey:PHONE_NUMBER]]];
                
                [self.navigationController popViewControllerAnimated:YES];
            } else {
                [self getNextFallback];
            }
        } else {
            if (error != nil) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                if (errorData != nil) {
                    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                    if (serializedData != nil) {
                        NSLog(@"%@", [serializedData valueForKey:@"error"]);
                    }
                }
            }
            
            [self getNextFallback];
        }
    }];
}

- (void)validateNumber {
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *number = [defaults valueForKey:TYPED_PHONE_NUMBER];
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:number forKey:API_PARAM_NUMBER];
    [dict setValue:API_CHECK_CLI forKey:API_PARAM_TYPE];
    [dict setValue:@"ios" forKey:API_PARAM_PLATFORM];
    
    [KVNProgress showWithStatus:@"Validating number"];
    [[CheckMobiAPI sharedClient] validateRequest:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            [KVNProgress dismiss];
            
            _validationKey = [result valueForKey:@"id"];
            _dialingNumber = [result valueForKey:@"dialing_number"];
            
            NSString *phoneURLString = [NSString stringWithFormat:@"telprompt://%@", _dialingNumber];
            NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
            [[UIApplication sharedApplication] openURL:phoneURL];
        } else {
            [self getNextFallback];
        }
    }];
}

- (void)getNextFallback {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:ENABLED_BACKUP_SMS]) {
        _type = API_CHECK_SMS;
    } else if ([defaults boolForKey:ENABLED_BACKUP_IVR]) {
        _type = API_CHECK_IVR;
    }
    
    if (([_type isEqualToString:API_CHECK_SMS]) && !triedSMS) {
        triedSMS = YES;
        [self fallbackForMethod:API_CHECK_SMS];
    } else if (([_type isEqualToString:API_CHECK_SMS]) && triedSMS && !triedIVR) {
        triedIVR = YES;
        [self fallbackForMethod:API_CHECK_IVR];
    } else if (([_type isEqualToString:API_CHECK_IVR]) && !triedIVR) {
        triedIVR = YES;
        [self fallbackForMethod:API_CHECK_IVR];
    } else if (([_type isEqualToString:API_CHECK_IVR]) && triedIVR && !triedSMS) {
        triedSMS = YES;
        [self fallbackForMethod:API_CHECK_SMS];
    } else {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
    }
}

- (void)fallbackForMethod:(NSString*)computedType {
    NSLog(@"fallbackForMethod %@", computedType);
    
    /*if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:@"No internet connection available!"];
        return;
    }*/
    
    _type = computedType;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *number = [defaults valueForKey:TYPED_PHONE_NUMBER];
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:_type forKey:API_PARAM_TYPE];
    [dict setValue:number forKey:API_PARAM_NUMBER];
    [dict setValue:@"ios" forKey:API_PARAM_PLATFORM];
    
    [[CheckMobiAPI sharedClient] validateRequest:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            _validationKey = [result valueForKey:@"id"];
            
            [KVNProgress dismiss];
            [self validatePin];
        } else {
            if (triedSMS && triedIVR) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
            } else {
                [self getNextFallback];
            }
        }
    }];
}

#pragma mark actions

- (void)sendSMSAction {
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    triedSMS = YES;
    [self fallbackForMethod:API_CHECK_SMS];
}

- (void)callAction {
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    triedIVR = YES;
    [self fallbackForMethod:API_CHECK_IVR];
}

- (void)validatePin {
    [self performSegueWithIdentifier:@"ValidatePinFromConfirmation" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ValidatePinFromConfirmation"]) {
        EnterCodeViewController *vc = [segue destinationViewController];
        vc.validationId = _validationKey;
        vc.validationMethod = _type;
    }
}

- (IBAction)navigateBack:(id)sender {
}

@end
