//
//  AFAPIClient.h
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

@interface CheckMobiAPI : AFHTTPRequestOperationManager

+ (instancetype)sharedClient;

- (void)validateCallNumber:(NSMutableDictionary*)dict andResponse:(void (^)(id result, NSError *error))block;
- (void)validateRequest:(NSMutableDictionary*)dict andResponse:(void (^)(id result, NSError *error))block;
- (void)getValidationStatus:(NSString*)validationKey andResponse:(void (^)(id result, NSError *error))block;
- (void)validateVerify:(NSMutableDictionary*)dict andResponse:(void (^)(id result, NSError *error))block;

@end
