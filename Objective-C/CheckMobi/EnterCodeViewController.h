//
//  EnterCodeViewController.h
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterCodeViewController : UIViewController

@property(nonatomic, weak) IBOutlet UILabel *msg;
@property(nonatomic, weak) IBOutlet UITextField *input;
@property(nonatomic, weak) IBOutlet UIButton *btn;
@property(nonatomic, weak) IBOutlet UILabel *errorMsg;
@property(nonatomic, weak) IBOutlet UIButton *resend;
@property(nonatomic, weak) IBOutlet UIButton *call;

@property(nonatomic, strong) NSString *validationId;
@property(nonatomic, strong) NSString *validationMethod;
@property(nonatomic) BOOL countingDown;

@end
