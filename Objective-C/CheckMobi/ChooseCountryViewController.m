//
//  ChooseCountryViewController.m
//  CheckMobi
//
//  Created by checkMobi on 16/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "ChooseCountryViewController.h"

@interface ChooseCountryViewController ()

@end

@implementation ChooseCountryViewController {
    NSMutableArray *countriesIndexedByLetter;
    NSMutableDictionary *countriesDict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Country", @"");
    self.countriesTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([self.countriesTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.countriesTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    NSLocale *locale = [NSLocale currentLocale];
    countriesIndexedByLetter = [NSMutableArray new];
    countriesDict = [NSMutableDictionary new];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    
    NSMutableArray *sortedCountryArray = [[NSMutableArray alloc] init];
    for (NSString *countryCode in countryArray) {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        [sortedCountryArray addObject:displayNameString];
    }
    
    [sortedCountryArray sortUsingSelector:@selector(localizedCompare:)];
    
    for (NSString *country in sortedCountryArray) {
        NSString *firstLetter =  [country substringToIndex:1];
        
        if (![countriesIndexedByLetter containsObject:firstLetter]) {
            [countriesIndexedByLetter addObject:firstLetter];
            if ([countriesDict valueForKey:firstLetter] == nil) {
                NSMutableArray *array = [NSMutableArray new];
                [array addObject:country];
                [countriesDict setValue:array forKey:firstLetter];
            } else {
                NSMutableArray *array = [countriesDict valueForKey:firstLetter];
                [array addObject:country];
                [countriesDict setValue:array forKey:firstLetter];
            }
        } else {
            NSMutableArray *array = [countriesDict valueForKey:firstLetter];
            [array addObject:country];
            [countriesDict setValue:array forKey:firstLetter];
        }
    }
    
    UIImage *backBtnImage = [UIImage imageNamed:@"icon-back"];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 20, backBtnImage.size.width + 10, 44);
    [closeButton setImage:backBtnImage forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    self.navigationItem.leftBarButtonItem = back;
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *firstLetter =  [[defaults valueForKey:SELECTED_COUNTRY_NAME] substringToIndex:1];
    NSInteger section = -1;
    NSInteger row = 0;
    for (NSString *letter in countriesIndexedByLetter) {
        section++;
        if ([letter isEqualToString:firstLetter]) {
            NSArray *countries = [countriesDict valueForKey:firstLetter];
            row = [countries indexOfObject:[defaults valueForKey:SELECTED_COUNTRY_NAME]];
            break;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    [_countriesTable scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - uitableview

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *sectionTitle = [countriesIndexedByLetter objectAtIndex:indexPath.section];
    NSArray *sectionCountries = [countriesDict objectForKey:sectionTitle];
    NSString *country = [sectionCountries objectAtIndex:indexPath.row];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:country forKey:SELECTED_COUNTRY_NAME];
    
    NSLocale *locale = [NSLocale currentLocale];
    NSArray *countryArray = [NSLocale ISOCountryCodes];
    for (NSString *countryCode in countryArray) {
        NSString *displayNameString = [locale displayNameForKey:NSLocaleCountryCode value:countryCode];
        if ([displayNameString isEqualToString:country]) {
            NSArray *countriesByCode = [COUNTRIES valueForKey:[countryCode uppercaseString]];
            if (countriesByCode.count == 2) {
                [defaults setValue:[NSString stringWithFormat:@"(+%@)", [countriesByCode objectAtIndex:1]] forKey:SELECTED_COUNTRY_PREFIX];
            }
            
            break;
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [countriesIndexedByLetter count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [countriesIndexedByLetter objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = [countriesIndexedByLetter objectAtIndex:section];
    NSArray *sectionCountries = [countriesDict objectForKey:sectionTitle];
    return [sectionCountries count];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return countriesIndexedByLetter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    static NSString *CellIdentifier = @"CellCountry";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [self tableviewCellCountryReuseIdentifier:CellIdentifier];
    }
    
    NSString *sectionTitle = [countriesIndexedByLetter objectAtIndex:indexPath.section];
    NSArray *sectionCountries = [countriesDict objectForKey:sectionTitle];
    UILabel *countryLabel = (UILabel*)[cell viewWithTag:1];
    
    NSString *country = [sectionCountries objectAtIndex:indexPath.row];
    countryLabel.text = country;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([country isEqualToString:[defaults valueForKey:SELECTED_COUNTRY_NAME]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (UITableViewCell*)tableviewCellCountryReuseIdentifier:(NSString*)identifier {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellAccessoryNone;
    
    UILabel *label = [UILabel new];
    label.tag = 1;
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor colorWithWhite:0 alpha:0.8];
    label.font = [UIFont fontWithName:NORMAL_FONT size:14];
    [cell.contentView addSubview:label];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return cell;
}

@end
