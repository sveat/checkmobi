//
//  ConfirmationViewController.h
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015checkMobiDS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCall.h>

@interface ConfirmationViewController : UIViewController

@property(nonatomic, weak) IBOutlet UILabel *msg;
@property(nonatomic, weak) IBOutlet UILabel *info;
@property(nonatomic, weak) IBOutlet UIButton *btn;
@property(nonatomic, weak) IBOutlet UIButton *sendSMS;
@property(nonatomic, weak) IBOutlet UIButton *call;

@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSString* callId;
@property (nonatomic, strong) NSString* dialingNumber;
@property (nonatomic, strong) CTCallCenter* callCenter;
@property (nonatomic, strong) NSString* validationKey;

@end
