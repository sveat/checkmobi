//
//  AboutViewController.h
//  CheckMobi
//
//  Created by checkMobi on 23/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property(nonatomic, weak) IBOutlet UILabel *version;
@property(nonatomic, weak) IBOutlet UILabel *link;

@end
