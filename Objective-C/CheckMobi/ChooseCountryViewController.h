//
//  ChooseCountryViewController.h
//  CheckMobi
//
//  Created by checkMobi on 16/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseCountryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *countriesTable;

@end
