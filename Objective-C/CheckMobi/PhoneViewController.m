//
//  PhoneViewController.m
//  CheckMobi
//
//  Created by checkMobi on 15/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "PhoneViewController.h"
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "HexColors.h"
#import "CheckMobiAPI.h"
#import "EnterCodeViewController.h"
#import <KVNProgress/KVNProgress.h>
#import "JSONResponseSerializerWithData.h"
#import "ViewController.h"

@interface PhoneViewController ()

@end

@implementation PhoneViewController {
    NSDictionary *countryCodes;
    NSString *phoneNumber;
    NSString *validationId;
    BOOL firstTime;
    UIButton *continueBtn;
    UITextField *phone;
    
    BOOL triedSMS;
    BOOL triedIVR;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHexString:DEFAULT_COLOR]];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self setupCountryCodes];
    firstTime = YES;
    self.title = NSLocalizedString(@"Setup", @"");
    self.settingsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([self.settingsTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.settingsTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults valueForKey:SELECTED_COUNTRY_NAME] == nil) {
        NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
        if (countryCode != nil) {
            NSArray *countryInfo = [countryCodes objectForKey:countryCode];
            if (countryInfo.count == 2) {
                [defaults setValue:[countryInfo objectAtIndex:0] forKey:SELECTED_COUNTRY_NAME];
                [defaults setValue:[NSString stringWithFormat:@"(+%@)", [countryInfo objectAtIndex:1]] forKey:SELECTED_COUNTRY_PREFIX];
            }
        }
    }
    
    if (_showSettingsIcon) {
        UIBarButtonItem *settings = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings"] style:UIBarButtonItemStylePlain target:self action:@selector(goToSettings)];
        self.navigationItem.rightBarButtonItem = settings;
    } else {
        UIImage *backBtnImage = [UIImage imageNamed:@"icon-back"];
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(0, 20, backBtnImage.size.width + 10, 44);
        [closeButton setImage:backBtnImage forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
        self.navigationItem.leftBarButtonItem = back;
    }
}

- (void)goBack {
    if ([phone isFirstResponder]) {
        [phone resignFirstResponder];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (void)goToSettings {
    if ([phone isFirstResponder]) {
        [phone resignFirstResponder];
    }
    
    [self performSegueWithIdentifier:@"SettingsBack" sender:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    triedSMS = NO || (![defaults valueForKey:ENABLED_BACKUP_SMS]);
    triedIVR = NO || (![defaults valueForKey:ENABLED_BACKUP_IVR]);
    
    [_settingsTable reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark uitableview

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"ChooseCountry" sender:self];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _settingsTable.frame.size.width, 40)];
    
    UILabel *label = [UILabel new];
    label.text = NSLocalizedString(@"EnterPhone", @"");
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:BOLD_FONT size:14];
    [header addSubview:label];
    
    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-5-[label]-5-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [header addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:header attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return header;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _settingsTable.frame.size.width, 90)];
    
    continueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    continueBtn.translatesAutoresizingMaskIntoConstraints = NO;
    [continueBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [continueBtn setTitleColor:[UIColor colorWithWhite:1 alpha:0.4] forState:UIControlStateDisabled];
    continueBtn.backgroundColor = [UIColor colorWithHexString:DEFAULT_COLOR];
    continueBtn.layer.cornerRadius = 5;
    continueBtn.clipsToBounds = YES;
    [continueBtn setTitle:NSLocalizedString(@"Continue", @"") forState:UIControlStateNormal];
    continueBtn.titleLabel.font = [UIFont fontWithName:NORMAL_FONT size:19];
    [continueBtn addTarget:self action:@selector(callSMSValidation) forControlEvents:UIControlEventTouchUpInside];
    [footer addSubview:continueBtn];
    
    if (phoneNumber.length > 0) {
        continueBtn.enabled = YES;
    } else {
        continueBtn.enabled = NO;
    }
    
    [footer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[continueBtn]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(continueBtn)]];
    [footer addConstraint:[NSLayoutConstraint constraintWithItem:continueBtn attribute:NSLayoutAttributeCenterY
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:footer attribute:NSLayoutAttributeCenterY
                                                      multiplier:1.0f constant:0]];
    [footer addConstraint:[NSLayoutConstraint constraintWithItem:continueBtn attribute:NSLayoutAttributeHeight
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1.0f constant:45]];
    
    return footer;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"CellCode";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [self tableviewCellCodeReuseIdentifier:CellIdentifier];
        }
        
        UILabel *label = (UILabel*)[cell viewWithTag:1];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        label.text = [NSString stringWithFormat:@"%@ %@", [defaults valueForKey:SELECTED_COUNTRY_NAME], [defaults valueForKey:SELECTED_COUNTRY_PREFIX]];
    } else {
        static NSString *CellIdentifier = @"CellPhone";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [self tableviewCellPhoneReuseIdentifier:CellIdentifier];
        }
        
        UITextField *phoneField = (UITextField*)[cell viewWithTag:1];
        if (firstTime) {
            [phoneField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:1.0f];
            firstTime = NO;
        }
    }
    
    return cell;
}

- (UITableViewCell*)tableviewCellPhoneReuseIdentifier:(NSString*)identifier {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    phone = [UITextField new];
    [phone addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    phone.tag = 1;
    phone.keyboardType = UIKeyboardTypeNumberPad;
    phone.placeholder = NSLocalizedString(@"NumberCode", @"");
    phone.font = [UIFont fontWithName:NORMAL_FONT size:14];
    phone.textColor = [UIColor blackColor];
    phone.translatesAutoresizingMaskIntoConstraints = NO;
    [cell.contentView addSubview:phone];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[phone]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(phone)]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:phone attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return cell;
}

- (UITableViewCell*)tableviewCellCodeReuseIdentifier:(NSString*)identifier {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor whiteColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *label = [UILabel new];
    label.tag = 1;
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor colorWithWhite:0 alpha:0.8];
    label.font = [UIFont fontWithName:NORMAL_FONT size:14];
    [cell.contentView addSubview:label];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[label]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return cell;
}

#pragma mark UITextfield

-(void)textFieldDidChange :(UITextField *)theTextField {
    phoneNumber = theTextField.text;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%@%@", [defaults valueForKey:SELECTED_COUNTRY_PREFIX], phoneNumber] forKey:TYPED_PHONE_NUMBER];
    
    if (phoneNumber.length > 0) {
        continueBtn.enabled = YES;
    } else {
        continueBtn.enabled = NO;
    }
}

#pragma mark utils

- (void)setupCountryCodes {
    countryCodes = COUNTRIES;
}

- (void)goToConfirmation {
    [self performSegueWithIdentifier:@"GoToConfirmation" sender:self];
}

- (void)validatePin {
    [self performSegueWithIdentifier:@"ValidatePin" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"ValidatePin"]) {
        EnterCodeViewController *vc = [segue destinationViewController];
        vc.validationId = validationId;
        vc.validationMethod = _type;
    } else if ([[segue identifier] isEqualToString:@"SettingsBack"]) {
        ViewController *vc = [segue destinationViewController];
        vc.fromMain = YES;
    }
}

- (IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {

}

- (void)callSMSValidation {
    if ([phone isFirstResponder]) {
        [phone resignFirstResponder];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (([defaults boolForKey:ENABLED_SMS_VERIFICATION]) || ([defaults boolForKey:ENABLED_IVR_CALL])) {
        NSString *prefix = [defaults valueForKey:SELECTED_COUNTRY_PREFIX];
        prefix = [prefix stringByReplacingOccurrencesOfString:@"(" withString:@""];
        prefix = [prefix stringByReplacingOccurrencesOfString:@")" withString:@""];
        NSString *message = [NSString stringWithFormat:@"We will be verifying the phone number %@%@. Is this OK, or would you like to edit the number?", prefix, phoneNumber];
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"Confirmation", @"")
                                              message:message
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *editAction = [UIAlertAction
                                       actionWithTitle:NSLocalizedString(@"Edit", @"")
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action) {
                                           [alertController dismissViewControllerAnimated:YES completion:nil];
                                       }];
        UIAlertAction *okAction = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Ok", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action) {
                                         [self codeVerificationMethods];
                                     }];
        
        [alertController addAction:editAction];
        [alertController addAction:okAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (([defaults boolForKey:ENABLED_CALLER_ID]) && (![defaults boolForKey:ENABLED_LOCAL_ONLY])) {
        [self validateCallNumber];
    } else if ([defaults boolForKey:ENABLED_MISSED_CALL]) {
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:NSLocalizedString(@"ReceiveMissedCall", @"")
                                              message:NSLocalizedString(@"MissedCallInfo", @"")
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *editAction = [UIAlertAction
                                     actionWithTitle:NSLocalizedString(@"Cancel", @"")
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action) {
                                         [alertController dismissViewControllerAnimated:YES completion:nil];
                                     }];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Ok", @"")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action) {
                                       [self codeVerificationMethods];
                                   }];
        
        [alertController addAction:editAction];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    } else if (([defaults boolForKey:ENABLED_CALLER_ID]) && ([defaults boolForKey:ENABLED_LOCAL_ONLY])) {
        [self checkLocalNumber];
    }
}

- (void)codeVerificationMethods {
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prefix = [defaults valueForKey:SELECTED_COUNTRY_PREFIX];
    prefix = [prefix stringByReplacingOccurrencesOfString:@"(" withString:@""];
    prefix = [prefix stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    if ([defaults boolForKey:ENABLED_MISSED_CALL]) {
        _type = API_CHECK_REV_CLI;
        [dict setValue:API_CHECK_REV_CLI forKey:API_PARAM_TYPE];
    } else if ([defaults boolForKey:ENABLED_SMS_VERIFICATION]) {
        triedSMS = YES;
        _type = API_CHECK_SMS;
        [dict setValue:API_CHECK_SMS forKey:API_PARAM_TYPE];
    } else if ([defaults boolForKey:ENABLED_IVR_CALL]) {
        triedIVR = YES;
        _type = API_CHECK_IVR;
        [dict setValue:API_CHECK_IVR forKey:API_PARAM_TYPE];
    }
    
    [dict setValue:[NSString stringWithFormat:@"%@%@", prefix, phoneNumber] forKey:API_PARAM_NUMBER];
    [dict setValue:@"ios" forKey:API_PARAM_PLATFORM];
    
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    [[CheckMobiAPI sharedClient] validateRequest:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            validationId = [result valueForKey:@"id"];
            NSString *formatting = [result valueForKeyPath:@"validation_info.formatting"];
            if (formatting != nil) {
                [defaults setValue:formatting forKey:PHONE_NUMBER];
            }
            
            [KVNProgress dismiss];
            [self validatePin];
        } else {
            if (error != nil) {
                NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                if (errorData != nil) {
                    NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                    if (serializedData != nil) {
                        NSLog(@"%@ %@", error, [serializedData valueForKey:@"error"]);
                    }
                }
            }
            
            [self getNextFallback];
        }
    }];
}

- (void)getNextFallback {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([_type isEqualToString:API_CHECK_SMS] && [defaults boolForKey:ENABLED_BACKUP_IVR] && !triedIVR) {
        _type = API_CHECK_IVR;
        [self fallback];
    } else if ([_type isEqualToString:API_CHECK_IVR] && [defaults boolForKey:ENABLED_BACKUP_SMS] && !triedSMS) {
        _type = API_CHECK_SMS;
        [self fallback];
    } else if (([_type isEqualToString:API_CHECK_REV_CLI]) || (([_type isEqualToString:API_CHECK_CLI]))) {
        if ([defaults boolForKey:ENABLED_BACKUP_SMS]) {
            _type = API_CHECK_SMS;
        } else if ([defaults boolForKey:ENABLED_BACKUP_IVR]) {
            _type = API_CHECK_IVR;
        }
        
        if (([_type isEqualToString:API_CHECK_SMS]) && !triedSMS) {
            triedSMS = YES;
            [self fallback];
        } else if (([_type isEqualToString:API_CHECK_SMS]) && triedSMS && !triedIVR) {
            triedIVR = YES;
            [self fallback];
        } else if (([_type isEqualToString:API_CHECK_IVR]) && !triedIVR) {
            triedIVR = YES;
            [self fallback];
        } else if (([_type isEqualToString:API_CHECK_IVR]) && triedIVR && !triedSMS) {
            triedSMS = YES;
            [self fallback];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
        }
    } else {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
    }
}

- (void)fallback {
    NSLog(@"fallback for %@", _type);
    
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prefix = [defaults valueForKey:SELECTED_COUNTRY_PREFIX];
    prefix = [prefix stringByReplacingOccurrencesOfString:@"(" withString:@""];
    prefix = [prefix stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:_type forKey:API_PARAM_TYPE];
    [dict setValue:[NSString stringWithFormat:@"%@%@", prefix, phoneNumber] forKey:API_PARAM_NUMBER];
    [dict setValue:@"ios" forKey:API_PARAM_PLATFORM];
    
    [[CheckMobiAPI sharedClient] validateRequest:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            validationId = [result valueForKey:@"id"];
            
            [KVNProgress dismiss];
            [self validatePin];
        } else {
            if (triedSMS && triedIVR) {
                [KVNProgress showErrorWithStatus:NSLocalizedString(@"FailedValidation", @"")];
            } else {
                [self getNextFallback];
            }
        }
    }];
}

- (void)checkLocalNumber {
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prefix = [defaults valueForKey:SELECTED_COUNTRY_PREFIX];
    prefix = [prefix stringByReplacingOccurrencesOfString:@"(" withString:@""];
    prefix = [prefix stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:[NSString stringWithFormat:@"%@%@", prefix, phoneNumber] forKey:API_PARAM_NUMBER];
    
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    [[CheckMobiAPI sharedClient] validateCallNumber:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            NSDictionary* cli = [result valueForKeyPath:@"validation_methods.cli"];
            if ([[cli valueForKey:@"local_number"] boolValue]) {
                [KVNProgress dismiss];
                [self goToConfirmation];
            } else {
                _type = API_CHECK_CLI;
                [self getNextFallback];
            }
        } else {
            /*_type = API_CHECK_CLI;
             [self getNextFallback];*/
            
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"InvalidNumber", @"")];
        }
    }];
}

- (void)validateCallNumber {
    if (![[Reachability reachabilityForInternetConnection] isReachable]) {
        [KVNProgress showErrorWithStatus:NSLocalizedString(@"NoInternet", @"")];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *number = [defaults valueForKey:TYPED_PHONE_NUMBER];
    number = [number stringByReplacingOccurrencesOfString:@"(" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    NSMutableDictionary *dict = [NSMutableDictionary new];
    [dict setValue:number forKey:API_PARAM_NUMBER];
    
    [KVNProgress showWithStatus:NSLocalizedString(@"Validating", @"")];
    [[CheckMobiAPI sharedClient] validateCallNumber:dict andResponse:^(id result, NSError *error) {
        if (result != nil) {
            [KVNProgress dismiss];
            [self goToConfirmation];
        } else {
            [KVNProgress showErrorWithStatus:NSLocalizedString(@"InvalidNumber", @"")];
        }
    }];
}

@end
