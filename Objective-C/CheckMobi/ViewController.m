//
//  ViewController.m
//  CheckMobi
//
//  Created by checkMobi on 14/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "ViewController.h"
#import "HexColors.h"
#import "ActionSheetPicker.h"
#import "JVFloatLabeledTextField.h"
#import <KVNProgress/KVNProgress.h>

@interface ViewController ()

@end

@implementation ViewController {
    NSString *editedValue;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"Settings", @"");
    self.settingsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    if ([self.settingsTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.settingsTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithHexString:DEFAULT_COLOR]];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    if (!_fromMain) {
        UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStylePlain target:self action:@selector(doneWihConfigurations)];
        self.navigationItem.rightBarButtonItem = done;
    } else {
        UIImage *backBtnImage = [UIImage imageNamed:@"icon-back"];
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(0, 20, backBtnImage.size.width + 10, 44);
        [closeButton setImage:backBtnImage forState:UIControlStateNormal];
        [closeButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
        self.navigationItem.leftBarButtonItem = back;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    editedValue = [defaults valueForKey:API_KEY];
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_settingsTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHidden:)
                                                 name:UIKeyboardDidHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    [self.settingsTable setContentInset:contentInsets];
    [self.settingsTable setScrollIndicatorInsets:contentInsets];
    NSIndexPath *currentRowIndex = [NSIndexPath indexPathForRow:0 inSection:3];
    [self.settingsTable scrollToRowAtIndexPath:currentRowIndex atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)keyboardWasHidden:(NSNotification*)aNotification {
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    [self.settingsTable setContentInset:contentInsets];
    [self.settingsTable setScrollIndicatorInsets:contentInsets];
}

- (void)doneWihConfigurations {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:editedValue forKey:API_KEY];
    
    [self performSegueWithIdentifier:@"PhoneSetup" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark uitextfield

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidChange :(UITextField *)theTextField {
    editedValue = theTextField.text;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:editedValue forKey:API_KEY];
}

#pragma mark uitableview

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            int initialSelection = 0;
            if ([defaults boolForKey:ENABLED_MISSED_CALL]) {
                initialSelection = 1;
            } else if ([defaults boolForKey:ENABLED_SMS_VERIFICATION]) {
                initialSelection = 2;
            } else if ([defaults boolForKey:ENABLED_IVR_CALL]) {
                initialSelection = 3;
            }
            
            NSArray *array = [NSArray arrayWithObjects:@"Caller ID", @"Missed Call", @"SMS", @"IVR", nil];
            [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"Method", @"") rows:array initialSelection:initialSelection doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                if (selectedIndex == 0) {
                    [defaults setBool:YES forKey:ENABLED_CALLER_ID];
                    [defaults setBool:NO forKey:ENABLED_IVR_CALL];
                    [defaults setBool:NO forKey:ENABLED_MISSED_CALL];
                    [defaults setBool:NO forKey:ENABLED_SMS_VERIFICATION];
                } else if (selectedIndex == 1) {
                    [defaults setBool:NO forKey:ENABLED_CALLER_ID];
                    [defaults setBool:NO forKey:ENABLED_IVR_CALL];
                    [defaults setBool:YES forKey:ENABLED_MISSED_CALL];
                    [defaults setBool:NO forKey:ENABLED_SMS_VERIFICATION];
                } else if (selectedIndex == 2) {
                    [defaults setBool:NO forKey:ENABLED_CALLER_ID];
                    [defaults setBool:NO forKey:ENABLED_IVR_CALL];
                    [defaults setBool:NO forKey:ENABLED_MISSED_CALL];
                    [defaults setBool:YES forKey:ENABLED_SMS_VERIFICATION];
                    [defaults setBool:YES forKey:ENABLED_BACKUP_SMS];
                } else if (selectedIndex == 3) {
                    [defaults setBool:NO forKey:ENABLED_CALLER_ID];
                    [defaults setBool:YES forKey:ENABLED_IVR_CALL];
                    [defaults setBool:NO forKey:ENABLED_MISSED_CALL];
                    [defaults setBool:NO forKey:ENABLED_SMS_VERIFICATION];
                    [defaults setBool:YES forKey:ENABLED_BACKUP_IVR];
                }
                
                [_settingsTable reloadData];
            } cancelBlock:nil origin:cell];
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 1) {
            NSArray *array = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", nil];
            [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"NoOfRetries", @"") rows:array initialSelection:2 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:[NSNumber numberWithInteger:[selectedValue integerValue]] forKey:SMS_NO_OF_RETRIES];
                
                [_settingsTable reloadData];
            } cancelBlock:nil origin:cell];
        } else if (indexPath.row == 2) {
            NSArray *array = [NSArray arrayWithObjects:@"0", @"10", @"20", @"30", @"40", @"50", @"60", @"70", @"80", @"90", @"100", @"110", @"120", @"130", @"140", @"150", @"160", @"170", @"180", nil];
            [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"SMSIn", @"") rows:array initialSelection:3 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:[NSNumber numberWithInteger:[selectedValue integerValue]] forKey:SMS_RETRY_IN];
                
                [_settingsTable reloadData];
            } cancelBlock:nil origin:cell];
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 1) {
            NSArray *array = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", nil];
            [ActionSheetStringPicker showPickerWithTitle:NSLocalizedString(@"NoOfRetries", @"") rows:array initialSelection:2 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:[NSNumber numberWithInteger:[selectedValue integerValue]] forKey:IVR_NO_OF_RETRIES];
                
                [_settingsTable reloadData];
            } cancelBlock:nil origin:cell];
        }
    } else if (indexPath.section == 4) {
        [self performSegueWithIdentifier:@"showAbout" sender:self];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    switch (section) {
        case 0:
            if ([defaults boolForKey:ENABLED_CALLER_ID]) {
                return 2;
            } else {
                return 1;
            }
            break;
        case 1:
            return 3;
            break;
        case 2:
            return 2;
            break;
    }
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ((section == 0) || (section == 1)) {
        return 44;
    }
    
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if ((section == 0) || (section == 2)) {
        return 50;
    }
    
    return 0.001;
}

- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *bck = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor lightGrayColor];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = [UIFont fontWithName:NORMAL_FONT size:13];
    [bck addSubview:label];
    
    [bck addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [bck addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                    relatedBy:NSLayoutRelationEqual
                                                       toItem:bck attribute:NSLayoutAttributeCenterY
                                                   multiplier:1.0f constant:0]];
    
    if (section == 0) {
        label.text = NSLocalizedString(@"SettingsMsg1", @"");
    } else if (section == 2) {
        label.text = NSLocalizedString(@"SettingsMsg2", @"");
    } else {
        label.text = @"";
    }
    
    return bck;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *bck = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    
    UILabel *label = [UILabel new];
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:BOLD_FONT size:17];
    [bck addSubview:label];
    
    [bck addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [bck addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeBottom
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:bck attribute:NSLayoutAttributeBottom
                                                      multiplier:1.0f constant:0]];
    
    if (section == 0) {
        label.text = NSLocalizedString(@"SetMethod", @"");
    } else if (section == 1) {
        label.text = NSLocalizedString(@"Backup", @"");
    } else {
        label.text = @"";
    }
    
    return bck;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (indexPath.section == 4) {
        static NSString *CellIdentifier = @"DisclosureCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [self tableviewCellDisclosureReuseIdentifier:CellIdentifier];
        }
        
        UILabel *label = (UILabel *)[cell viewWithTag:1];
        label.text = @"About";
        label = (UILabel *)[cell viewWithTag:2];
        label.text = @"";
    } else if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            static NSString *CellIdentifier = @"DisclosureCell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [self tableviewCellDisclosureReuseIdentifier:CellIdentifier];
            }
            
            UILabel *label = (UILabel *)[cell viewWithTag:1];
            if ([defaults boolForKey:ENABLED_MISSED_CALL]) {
                label.text = @"Missed Call";
            } else if ([defaults boolForKey:ENABLED_SMS_VERIFICATION]) {
                label.text = @"SMS";
            } else if ([defaults boolForKey:ENABLED_IVR_CALL]) {
                label.text = @"IVR";
            } else {
                label.text = @"Caller ID";
            }
        } else {
            static NSString *CellIdentifier = @"CheckCell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [self tableviewCellCheckReuseIdentifier:CellIdentifier];
            }
            
            UILabel *label = (UILabel *)[cell viewWithTag:1];
            UISwitch *option = (UISwitch *)[cell viewWithTag:2];
            label.text = NSLocalizedString(@"LocalOnly", @"");
            [option setOn:[defaults boolForKey:ENABLED_LOCAL_ONLY]];
        }
    } else if (indexPath.section == 3) {
        static NSString *CellIdentifier = @"InputCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [self tableviewCellInputReuseIdentifier:CellIdentifier];
        }
        
        JVFloatLabeledTextField *label = (JVFloatLabeledTextField *)[cell viewWithTag:1];
        label.text = [defaults valueForKey:API_KEY];
    } else if (indexPath.row == 0) {
        static NSString *CellIdentifier = @"CheckCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [self tableviewCellCheckReuseIdentifier:CellIdentifier];
        }
        
        UILabel *label = (UILabel *)[cell viewWithTag:1];
        UISwitch *option = (UISwitch *)[cell viewWithTag:2];
        
        if (indexPath.section == 1) {
            label.text = @"SMS verification";
            [option setOn:[defaults boolForKey:ENABLED_BACKUP_SMS]];
            
            if ([defaults boolForKey:ENABLED_SMS_VERIFICATION]) {
                option.enabled = NO;
            } else {
                option.enabled = YES;
            }
        } else if (indexPath.section == 2) {
            label.text = @"IVR Call";
            [option setOn:[defaults boolForKey:ENABLED_BACKUP_IVR]];
            
            if ([defaults boolForKey:ENABLED_IVR_CALL]) {
                option.enabled = NO;
            } else {
                option.enabled = YES;
            }
        }
    } else {
        static NSString *CellIdentifier = @"DisclosureCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [self tableviewCellDisclosureReuseIdentifier:CellIdentifier];
        }
        
        UILabel *label = (UILabel *)[cell viewWithTag:1];
        UILabel *value = (UILabel *)[cell viewWithTag:2];
        
        if (indexPath.section == 1) {
            if (indexPath.row == 1) {
                label.text = NSLocalizedString(@"NoOfRetries", @"");
                value.text = [NSString stringWithFormat:@"%@", [defaults valueForKey:SMS_NO_OF_RETRIES]];
            } else {
                label.text = NSLocalizedString(@"SMSIn", @"");
                value.text = [NSString stringWithFormat:@"%@ seconds", [defaults valueForKey:SMS_RETRY_IN]];
            }
        } else if (indexPath.section == 2) {
            label.text = NSLocalizedString(@"NoOfRetries", @"");
            value.text = [NSString stringWithFormat:@"%@", [defaults valueForKey:IVR_NO_OF_RETRIES]];
        }
    }
    
    return cell;
}

- (UITableViewCell*)tableviewCellInputReuseIdentifier:(NSString*)identifier {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor whiteColor];
    
    JVFloatLabeledTextField *label = [JVFloatLabeledTextField new];
    [label addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    label.delegate = self;
    label.tag = 1;
    label.returnKeyType = UIReturnKeyDone;
    label.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"API key"
                                    attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:NORMAL_FONT size:14];
    [cell.contentView addSubview:label];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label)]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return cell;
}

- (UITableViewCell*)tableviewCellCheckReuseIdentifier:(NSString*)identifier {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [UILabel new];
    label.tag = 1;
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:NORMAL_FONT size:14];
    [cell.contentView addSubview:label];
    
    UISwitch *enableOption = [UISwitch new];
    enableOption.tag = 2;
    enableOption.translatesAutoresizingMaskIntoConstraints = NO;
    [enableOption addTarget:self action:@selector(optionChanged:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:enableOption];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-[enableOption]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label, enableOption)]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:enableOption attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return cell;
}

- (UITableViewCell*)tableviewCellDisclosureReuseIdentifier:(NSString*)identifier {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    cell.backgroundColor = [UIColor whiteColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    UILabel *label = [UILabel new];
    label.tag = 1;
    label.textAlignment = NSTextAlignmentLeft;
    label.translatesAutoresizingMaskIntoConstraints = NO;
    label.textColor = [UIColor blackColor];
    label.font = [UIFont fontWithName:NORMAL_FONT size:14];
    [cell.contentView addSubview:label];
    
    UILabel *value = [UILabel new];
    value.tag = 2;
    value.textAlignment = NSTextAlignmentRight;
    value.translatesAutoresizingMaskIntoConstraints = NO;
    value.textColor = [UIColor colorWithWhite:0 alpha:0.8];
    value.font = [UIFont fontWithName:NORMAL_FONT size:14];
    [cell.contentView addSubview:value];
    
    [cell.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-[value]-10-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(label, value)]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:label attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:value attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:cell.contentView attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0f constant:0]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)optionChanged:(UISwitch*)sender {
    UITableViewCell *cell = (UITableViewCell *)sender.superview.superview;
    NSIndexPath *indexPath = [_settingsTable indexPathForCell:cell];
    
    if (indexPath.section == 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:sender.on forKey:ENABLED_LOCAL_ONLY];
        
        [_settingsTable reloadData];
    } else if (indexPath.section == 1) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:sender.on forKey:ENABLED_BACKUP_SMS];
        [_settingsTable reloadData];
    } else if (indexPath.section == 2) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setBool:sender.on forKey:ENABLED_BACKUP_IVR];
        [_settingsTable reloadData];
    }
}

@end
