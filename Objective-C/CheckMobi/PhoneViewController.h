//
//  PhoneViewController.h
//  CheckMobi
//
//  Created by checkMobi on 15/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) IBOutlet UITableView *settingsTable;
@property(nonatomic, strong) NSString *type;
@property(nonatomic) BOOL showSettingsIcon;

@end
