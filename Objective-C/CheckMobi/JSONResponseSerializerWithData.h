//
//  JSONResponseSerializerWithData.h
//  CheckMobi
//
//  Created by checkMobi on 17/10/15.
//  Copyright © 2015 checkMobi. All rights reserved.
//

#import "AFURLResponseSerialization.h"

/// NSError userInfo key that will contain response data
static NSString * const JSONResponseSerializerWithDataKey = @"JSONResponseSerializerWithDataKey";

@interface JSONResponseSerializerWithData : AFJSONResponseSerializer

@end
